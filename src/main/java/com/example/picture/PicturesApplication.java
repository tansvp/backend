package com.example.picture;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing

public class PicturesApplication {

	public static void main(String[] args) {
		SpringApplication.run(PicturesApplication.class, args);
	}

}
