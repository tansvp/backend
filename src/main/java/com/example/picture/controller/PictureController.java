package com.example.picture.controller;

import com.example.picture.repository.PictureRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import com.example.picture.exception.ResourceNotFoundException;
import com.example.picture.model.Picture;



@Service
@CrossOrigin(origins = "http://localhost:4200" , maxAge = 3600)  //ส่งข้อมูลให้เชื่อมกับ front

@RestController
@RequestMapping("/api")

public class PictureController {

    @Autowired
    PictureRepository pictureRepository;

    // Get All Pictures
    @GetMapping("/pictures")
    public List<Picture> getAllPicture() {
        return pictureRepository.findAll();
    }

    // Create a new Picture
    @PostMapping("/pictures")
    public Picture createPicture(@Valid @RequestBody Picture picture) {
        return pictureRepository.save(picture);
    }

    // Get a Single Picture
    @GetMapping("/pictures/{id}")
    public Picture getPictureById(@PathVariable(value = "id") Long pictureId) {
        return pictureRepository.findById(pictureId)
                .orElseThrow(() -> new ResourceNotFoundException("Picture", "id", pictureId));
    }

    // Update a Picture
    @PutMapping("/pictures/{id}")
    public Picture updatePicture(@PathVariable(value = "id") Long pictureId,
                           @Valid @RequestBody Picture pictureDetails) {

        Picture picture = pictureRepository.findById(pictureId)
                .orElseThrow(() -> new ResourceNotFoundException("Picture", "id", pictureId));

        picture.setId(pictureDetails.getId());
        picture.setDetail(pictureDetails.getDetail());
        picture.setName(pictureDetails.getName());
        picture.setCamera(pictureDetails.getCamera());


        Picture updatedPicture = pictureRepository.save(picture);
        return updatedPicture;
    }

    // Delete a Picture
    @DeleteMapping("/pictures/{id}")
    public ResponseEntity<?> deletePicture(@PathVariable(value = "id") Long pictureId) {
        Picture picture = pictureRepository.findById(pictureId)
                .orElseThrow(() -> new ResourceNotFoundException("Picture", "id", pictureId));

        pictureRepository.delete(picture);

        return ResponseEntity.ok().build();
    }
}