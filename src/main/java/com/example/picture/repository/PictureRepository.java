package com.example.picture.repository;


import com.example.picture.model.Picture;

import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.JpaRepository;




@Repository
public interface PictureRepository extends JpaRepository <Picture, Long> {

}