package com.example.picture.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Date;
import java.io.Serializable;



@Entity
@Table(name = "pictures")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"},
        allowGetters = true)

public class Picture implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private Long id;

    @NotBlank
    private String detail;

    @NotBlank
    private String name;

    @NotBlank
    private String camera;




    @Column(nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date createdAt;

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    private Date updatedAt;



    public Long   getId()       { return id;}

    public String getDetail()   {return detail;}

    public String getName()   {return name;}


    public String getCamera()   {return camera;}


    public void setId(Long id)          { this.id = id;}

    public void setDetail(String detail) { this.detail = detail;}

    public void setName (String name) { this.name =name;}

    public void setCamera(String camera) { this.camera = camera;}

    // Getters and Setters ... (Omitted for brevity)
}